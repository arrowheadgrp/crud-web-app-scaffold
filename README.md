## Cloning
Open a new workspace in Eclipse and navigate to File -> Import to begin the process for cloning:
`https://bitbucket.org/arrowheadgrp/crud-web-app-scaffold.git`

After clonining into your workspace, you should have a Maven project named "crud-web-app-scaffold"

## Setup
After cloning app, right click on project "web-app" in Eclipse IDE and navigate to "Properties" -> "Project Facets". Ensure "Dynamic Web Module" is checked and is set to 3.1. If you cannot set it to 3.1, you'll most likely have to edit the file in ".settings/org.eclipse.wst.common.project.facet.core.xml" and change the line to read `<installed facet="jst.web" version="3.1"/>` and refresh your workspace (Window -> Show View -> Navigator to view files on system). You should be able to add a server runtime to your "Dynamic Web Module."

Setting your project to be a dynamic web module enables you to create a Tomcat 9.0 server in your Eclipse IDE in the "Servers" tab. If you completed the process above while you had an already created server configuration window open, close and reopen. Navigate to the "Modules" tab at the bottom of the server config window and select "Add Web Modules" and add "web-app" with path set to "/".  Save config settings (CTRL + S).

With default Tomcat Server settings, you should be able to start your server. Once the application has started, you should be able to navigate to `http://localhost:8080` and see a web page that says "Welcome to the..."
