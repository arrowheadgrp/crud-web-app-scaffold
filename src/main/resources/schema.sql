DROP TABLE IF EXISTS SicCode;

CREATE TABLE SicCode (
	SicCodeID integer auto_increment primary key,
	SicCode varchar(4) null,
	SicDesc varchar(500) null,
	IndustryGroupID int null,
	HazardGradeAuto int null,
	HazardGradeWC int null,
	PackageAppetiteAuto int null,
	PackageAppetiteWC int null,
	PackageAppetiteProp int null,
	PackageAppetiteGLPrem int null,
	PackageAppetiteGLProd int null,
	HazardGradeProp int null,
	HazardGradeGLPrem int null,
	HazardGradeGlProp int null,
	ClassAppetite varchar(20) null,
	InsertedDate datetime null,
	ModifiedDate datetime null
);
